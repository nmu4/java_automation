package nmu;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.parsing.Parser;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PetstoreTest {
    private String baseUrl = "https://petstore.swagger.io/v2";
    private static final String STORE = "/store";
    private static final String INVENTORY = STORE + "/inventory";
    private static final String ORDER = STORE + "/order";
    private static final String ORDER_ID = ORDER + "/{orderId}";

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        ResponseSpecification responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyGetInventoryAction() {
        given().get(INVENTORY)
                .then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void verifyPlaceOrderAction() {
        given().body("{" +
                        "\"id\": 1," +
                        "\"petId\": 1," +
                        "\"quantity\": 1," +
                        "\"shipDate\": \"2024-02-20T07:47:16.702Z\"," +
                        "\"status\": \"placed\"," +
                        "\"complete\": true" +
                        "}").post(ORDER)
                .then().statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyPlaceOrderAction")
    public void verifyGetOrderAction() {
        given().pathParam("orderId", 1)
                .when().get(ORDER_ID)
                .then().statusCode(HttpStatus.SC_OK)
                .and().body("id", equalTo(1));
    }

    @Test(dependsOnMethods = "verifyGetOrderAction")
    public void verifyUpdateOrderAction() {
        given().pathParam("orderId", 1)
                .body("{" +
                        "\"id\": 1," +
                        "\"petId\": 1," +
                        "\"quantity\": 2," +
                        "\"shipDate\": \"2024-02-20T07:47:16.702Z\"," +
                        "\"status\": \"placed\"," +
                        "\"complete\": true" +
                        "}").put(ORDER_ID)
                .then().statusCode(HttpStatus.SC_OK);
    }
}
