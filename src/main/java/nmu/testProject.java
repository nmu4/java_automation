package nmu;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration; // Import Duration class

public class testProject {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "/Users/local/apache-maven-3.9.6");

        // Ініціалізація драйвера Chrome
        WebDriver driver = new ChromeDriver();

        // Відкриття веб-сайту Zalando
        driver.get("https://www.zalando.com/");

        try {
            // Знаходження поле пошуку за ідентифікатором
            WebElement searchInput = driver.findElement(By.xpath("//input[@id='header-search-input']"));

            // Введення значення "pants" у поле пошуку
            searchInput.sendKeys("pants");

            // Натискання клавіші Enter у полі пошуку
            searchInput.sendKeys(Keys.RETURN);

            // Перевірка переадресації
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10)); // Use Duration object
            wait.until(ExpectedConditions.urlContains("pants"));
            System.out.println("Переадресація відбулася успішно.");

            // Перевірка видимості принаймні одного елементу
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".cat_articleCard___5Zg89")));
            System.out.println("Принаймні один елемент є видимим на сторінці.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Завершення сеансу браузера
            driver.quit();
        }
    }
}
